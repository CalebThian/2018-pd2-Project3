#include "player.h"
#define player_num 4
#define Amakusa 0
#define Emiya 1
//#define Helen 2
#define Illya 2
#define Kuro 3
#define AmakusaUlti1 4
#define AmakusaUlti2 5
#define EmiyaUlti 6
#define IllyaUlti 7
#define KuroUlti 8
#define sSound_num 2
//#define Scale 0.1
#define hpadS 5

extern Game * game;

Player::Player():Character()
{
    SSsound = new QMediaPlayer();
    rand_ply=rand()%player_num;
    int rand_sply = rand()%sSound_num;

    switch (rand_ply)
    {
        case Amakusa:
        {
            QPixmap * playerPic = new QPixmap(":/amakusa/Amakusa2.png");
            QPixmap * playerPicScaled = new QPixmap();
            //qDebug()<<"Amakusa is created by"<<playerPic->width()<<" "<<playerPic->height()<<endl;
            //int picw=playerPic->width()*Scale;
            //int pich=playerPic->height()*Scale;
            *playerPicScaled = playerPic->scaled(playerPic->width()*Scale,playerPic->height()*Scale,Qt::KeepAspectRatio);
            //qDebug()<<"Amakusa is created by"<<playerPicScaled->width()<<" "<<playerPicScaled->height()<<endl;
            setPixmap(*playerPicScaled);
            delete playerPic;
            delete playerPicScaled;
            //setPixmap(QPixmap(":/amakusa/Amakusa2.png"));
            //setScale(Scale);
            //qDebug()<<"Amakusa is created at"<<this->pixmap().width()<<" "<<this->pixmap().height()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/amakusa/AmakusaS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/amakusa/AmakusaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=120*hpadS;
            ad=5*hpadS;
            //qDebug()<<"Character initialization is success"<<endl;
        break;
        }

        case Emiya:
        {
            QPixmap * playerPic = new QPixmap(":/emiya/Emiya2.png");
            QPixmap * playerPicScaled = new QPixmap();
            //qDebug()<<"Amakusa is created by"<<playerPic->width()<<" "<<playerPic->height()<<endl;
            //int picw=playerPic->width()*Scale;
            //int pich=playerPic->height()*Scale;
            *playerPicScaled = playerPic->scaled(playerPic->width()*Scale,playerPic->height()*Scale,Qt::KeepAspectRatio);
            //qDebug()<<"Amakusa is created by"<<playerPicScaled->width()<<" "<<playerPicScaled->height()<<endl;
            setPixmap(*playerPicScaled);
            delete playerPic;
            delete playerPicScaled;
            //setPixmap((QPixmap(":/emiya/Emiya2.png")));
            //setScale(Scale);
            //qDebug()<<"Emiya is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/emiya/Emiya.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/emiya/EmiyaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=100*hpadS;
            ad=10*hpadS;
        break;
        }

        /*case Helen:
        {
            setPixmap((QPixmap(":/helen/Helen2.png")));
            setScale(Scale);
            qDebug()<<"Helen is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/helen/HelenS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/helen/HelenS2.m4a"));
                break;
            }
            plyStart->play();
            break;
        }*/

        case Illya:
        {
            QPixmap * playerPic = new QPixmap(":/illya/Illya2.png");
            QPixmap * playerPicScaled = new QPixmap();
            //qDebug()<<"Amakusa is created by"<<playerPic->width()<<" "<<playerPic->height()<<endl;
            //int picw=playerPic->width()*Scale;
            //int pich=playerPic->height()*Scale;
            *playerPicScaled = playerPic->scaled(playerPic->width()*Scale,playerPic->height()*Scale,Qt::KeepAspectRatio);
            //qDebug()<<"Amakusa is created by"<<playerPicScaled->width()<<" "<<playerPicScaled->height()<<endl;
            setPixmap(*playerPicScaled);
            delete playerPic;
            delete playerPicScaled;
            //setPixmap((QPixmap(":/illya/Illya2.png")));
            //setScale(Scale);
            //qDebug()<<"Illya is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/illya/IllyaS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/illya/IllyaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=80*hpadS;
            ad=15*hpadS;
        break;
        }

        case Kuro:
        {
            QPixmap * playerPic = new QPixmap(":/kuro/Kuro2.png");
            QPixmap * playerPicScaled = new QPixmap();
            //qDebug()<<"Amakusa is created by"<<playerPic->width()<<" "<<playerPic->height()<<endl;
            //int picw=playerPic->width()*Scale;
            //int pich=playerPic->height()*Scale;
            *playerPicScaled = playerPic->scaled(playerPic->width()*Scale,playerPic->height()*Scale,Qt::KeepAspectRatio);
            //qDebug()<<"Amakusa is created by"<<playerPicScaled->width()<<" "<<playerPicScaled->height()<<endl;
            setPixmap(*playerPicScaled);
            delete playerPic;
            delete playerPicScaled;
            //setPixmap((QPixmap(":/kuro/Kuro2.png")));
            //setScale(Scale);
            //qDebug()<<"Kuro is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/kuro/KuroS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/kuro/KuroS2.m4a"));
                break;
            }
            plyStart->play();
            hp=80*hpadS;
            ad=15*hpadS;
        }
    }

    posx=this->x()+this->pixmap().width()/2;
    posy=this->y()+this->pixmap().height()/2;
}

void Player::shoot()
{
    Bullet * b = new Bullet(this->x()+this->pixmap().width()/2/**Scale*/,this->y()+this->pixmap().height()/2/*Scale*/,rand_ply,ad,bulletBool);
    scene()->addItem(b);
}

void Player::skill1()
{
    if(game->skt1->getCounter()==0)
    {
        game->skt1->initialize();
        for(int i=0;i<10;++i)
            game->np->increaseNP();
        QTimer * timer =new QTimer();
        connect(timer,SIGNAL(timeout()),this,SLOT(count1Decrease()));
        timer->start(1000);
    }
}

void Player::skill2()
{
    if(game->skt2->getCounter()==0)
    {
        game->skt2->initialize();
        for(int i=0;i<20;++i)
            game->health->increase();
        game->hpblock->setRect(game->hpx,game->hpy,game->health->getHealth(),20);
        QTimer * timer =new QTimer();
        connect(timer,SIGNAL(timeout()),this,SLOT(count2Decrease()));
        timer->start(1000);
    }
}

void Player::ultimate()
{
    if(game->np->getnp()==100)
    {
        switch(rand_ply)
        {
        case Amakusa:
        {
            Bullet * amakusaUlti1 = new Bullet(0-200,0,AmakusaUlti1,100,bulletBool);
            Bullet * amakusaUlti2 = new Bullet(0-200,game->h-400,AmakusaUlti2,100,bulletBool);
            game->scene->addItem(amakusaUlti1);
            game->scene->addItem(amakusaUlti2);
            SSsound->setMedia(QUrl("qrc:/amakusa/AmakusaSS.m4a"));
            if(SSsound->state()==QMediaPlayer::PlayingState)
                SSsound->setPosition(0);
            else if (SSsound->state()==QMediaPlayer::StoppedState)
                SSsound->play();
            break;
        }
        case Emiya:
        {
            Bullet * b1 = new Bullet(0-1*500,0,EmiyaUlti,100,bulletBool);
            Bullet * b2 = new Bullet(0-1*500,200,EmiyaUlti,100,bulletBool);
            Bullet * b3 = new Bullet(0-1*500,400,EmiyaUlti,100,bulletBool);
            Bullet * b4 = new Bullet(0-1*500,600,EmiyaUlti,100,bulletBool);
            Bullet * b5 = new Bullet(0-2*500,0,EmiyaUlti,100,bulletBool);
            Bullet * b6 = new Bullet(0-2*500,200,EmiyaUlti,100,bulletBool);
            Bullet * b7 = new Bullet(0-2*500,400,EmiyaUlti,100,bulletBool);
            Bullet * b8 = new Bullet(0-2*500,600,EmiyaUlti,100,bulletBool);
            Bullet * b9 = new Bullet(0-3*500,0,EmiyaUlti,100,bulletBool);
            Bullet * b10 = new Bullet(0-3*500,200,EmiyaUlti,100,bulletBool);
            Bullet * b11 = new Bullet(0-3*500,400,EmiyaUlti,100,bulletBool);
            Bullet * b12 = new Bullet(0-3*500,600,EmiyaUlti,100,bulletBool);
            Bullet * b13 = new Bullet(0-4*500,0,EmiyaUlti,100,bulletBool);
            Bullet * b14 = new Bullet(0-4*500,200,EmiyaUlti,100,bulletBool);
            Bullet * b15 = new Bullet(0-4*500,400,EmiyaUlti,100,bulletBool);
            Bullet * b16 = new Bullet(0-4*500,600,EmiyaUlti,100,bulletBool);
            Bullet * b17 = new Bullet(0-5*500,0,EmiyaUlti,100,bulletBool);
            Bullet * b18 = new Bullet(0-5*500,200,EmiyaUlti,100,bulletBool);
            Bullet * b19 = new Bullet(0-5*500,400,EmiyaUlti,100,bulletBool);
            Bullet * b20 = new Bullet(0-5*500,600,EmiyaUlti,100,bulletBool);
            game->scene->addItem(b1);
            game->scene->addItem(b2);
            game->scene->addItem(b3);
            game->scene->addItem(b4);
            game->scene->addItem(b5);
            game->scene->addItem(b6);
            game->scene->addItem(b7);
            game->scene->addItem(b8);
            game->scene->addItem(b9);
            game->scene->addItem(b10);
            game->scene->addItem(b11);
            game->scene->addItem(b12);
            game->scene->addItem(b13);
            game->scene->addItem(b14);
            game->scene->addItem(b15);
            game->scene->addItem(b16);
            game->scene->addItem(b17);
            game->scene->addItem(b18);
            game->scene->addItem(b19);
            game->scene->addItem(b20);
            SSsound->setMedia(QUrl("qrc:/emiya/EmiyaSS.m4a"));
            if(SSsound->state()==QMediaPlayer::PlayingState)
                SSsound->setPosition(0);
            else if (SSsound->state()==QMediaPlayer::StoppedState)
                SSsound->play();
            break;
        }
        case Illya:
        {
            Bullet * illyaUlti = new Bullet(0,rand()%game->h-600,IllyaUlti,100,bulletBool);
            game->scene->addItem(illyaUlti);
            SSsound->setMedia(QUrl("qrc:/illya/IllyaSS.m4a"));
            if(SSsound->state()==QMediaPlayer::PlayingState)
                SSsound->setPosition(0);
            else if (SSsound->state()==QMediaPlayer::StoppedState)
                SSsound->play();
            break;
        }

        case Kuro:
        {
            Bullet * a1 = new Bullet(0,100,KuroUlti,100,bulletBool);
            Bullet * a2 = new Bullet(0,210,KuroUlti,100,bulletBool);
            Bullet * a3 = new Bullet(0,350,KuroUlti,100,bulletBool);
            Bullet * a4 = new Bullet(0,460,KuroUlti,100,bulletBool);
            Bullet * a5 = new Bullet(0,590,KuroUlti,100,bulletBool);
            Bullet * a6 = new Bullet(0,700,KuroUlti,100,bulletBool);
            game->scene->addItem(a1);
            game->scene->addItem(a2);
            game->scene->addItem(a3);
            game->scene->addItem(a4);
            game->scene->addItem(a5);
            game->scene->addItem(a6);
            break;
            SSsound->setMedia(QUrl("qrc:/kuro/KuroSS.m4a"));
            if(SSsound->state()==QMediaPlayer::PlayingState)
                SSsound->setPosition(0);
            else if (SSsound->state()==QMediaPlayer::StoppedState)
                SSsound->play();
            break;
        }
        }
    game->np->initialize();
    }
}

void Player::count1Decrease()
{
    game->skt1->decrease();
}

void Player::count2Decrease()
{
    game->skt2->decrease();
}

