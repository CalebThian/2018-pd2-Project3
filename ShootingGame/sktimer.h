#ifndef SKTIMER_H
#define SKTIMER_H

#include <QGraphicsTextItem>
#include <QFont>
#include <QTimer>
#include <QDebug>

class SkTimer: public QGraphicsTextItem
{
public:
    SkTimer(int a);
    void initialize();
    int getInitial();
    int getCounter();
    void decrease();
    void cannot();
    void can();

private:
    int initial;
    int counter;
};

#endif // SKTIMER_H
