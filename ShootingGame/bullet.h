#ifndef BULLET_H
#define BULLET_H
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QTimer>
#include <QObject>
#include <QGraphicsScene>
#include <QMediaPlayer>
#include <QList>
#include <QDebug>
#include <cstdlib>
#include <iostream>
//#include "enemy.h"
#include "game.h"

class Game;

class Bullet : public QObject, public QGraphicsPixmapItem
{
   Q_OBJECT

public:
    Bullet();
    Bullet(int a, int b, int c,int d,bool e);
    void HitMusic(int a);
    ~Bullet();

public slots:
    void move();

private:
    const int bwidth = 1;
    const int bheight = 1;
    int bNo = 0 ;
    int attack;
    int npIncrement;
    bool playerOrBoss;
    int orix;
    int oriy;

    const double amakusaBulletScale = 0.25;
    const double emiyaBulletScale = 0.25;
    const double illyaBulletScale = 0.1;
    const double kuroBulletcScale = 0.1;
    QPixmap * amakusaBullet = new QPixmap(":/amakusa/AmakusaBullet.png");
    QPixmap * emiyaBullet = new QPixmap(":/emiya/EmiyaBullet.png");
    QPixmap * illyaBullet = new QPixmap(":/bg/purpleflare.png");
    QPixmap * kuroBullet = new QPixmap(":/kuro/KuroBullet.png");
    QPixmap * amakusaUltiBulletScale1 = new QPixmap(":/bg/blueflare.png");
    QPixmap * amakusaUltiBulletScale2 = new QPixmap(":/bg/redflare.png");
    const  QPixmap * amakusaBulletScaled = new QPixmap(amakusaBullet->scaled(amakusaBullet->width()*amakusaBulletScale,amakusaBullet->height()*amakusaBulletScale,Qt::KeepAspectRatio));
    const  QPixmap * bAmakusaBulletScaled = new QPixmap(amakusaBullet->scaled(amakusaBullet->width()*amakusaBulletScale*2,amakusaBullet->height()*amakusaBulletScale*2,Qt::KeepAspectRatio));
    const  QPixmap * emiyaBulletScaled = new QPixmap(emiyaBullet->scaled(emiyaBullet->width()*emiyaBulletScale,emiyaBullet->height()*emiyaBulletScale,Qt::KeepAspectRatio));
    const  QPixmap * bEmiyaBulletScaled = new QPixmap(emiyaBullet->scaled(emiyaBullet->width()*emiyaBulletScale*2,emiyaBullet->height()*emiyaBulletScale*2,Qt::KeepAspectRatio));
    const  QPixmap * illyaBulletScaled = new QPixmap(illyaBullet->scaled(illyaBullet->width()*illyaBulletScale,illyaBullet->height()*illyaBulletScale,Qt::KeepAspectRatio));
    const  QPixmap * bIllyaBulletScaled = new QPixmap(illyaBullet->scaled(illyaBullet->width()*illyaBulletScale*2,illyaBullet->height()*illyaBulletScale*2,Qt::KeepAspectRatio));
    const  QPixmap * kuroBulletcScaled = new QPixmap(kuroBullet->scaled(kuroBullet->width()*kuroBulletcScale,kuroBullet->height()*kuroBulletcScale,Qt::KeepAspectRatio));
    const  QPixmap * bKuroBulletcScaled = new QPixmap(kuroBullet->scaled(kuroBullet->width()*kuroBulletcScale*2,kuroBullet->height()*kuroBulletcScale*2,Qt::KeepAspectRatio));
    const  QPixmap * amakusaUltiBulletScaled1 = new QPixmap(amakusaUltiBulletScale1->scaled(300,300,Qt::IgnoreAspectRatio));
    const  QPixmap * amakusaUltiBulletScaled2 = new QPixmap(amakusaUltiBulletScale2->scaled(300,300,Qt::IgnoreAspectRatio));
    const  QPixmap * emiyaUltiBulletScaled = new QPixmap(emiyaBullet->scaled(400,150,Qt::KeepAspectRatio));
    const  QPixmap * illyaUltiBulletScaled = new QPixmap(illyaBullet->scaled(600,600,Qt::IgnoreAspectRatio));
    const  QPixmap * kuroUltiBulletScaled = new QPixmap(kuroBullet->scaled(300,105,Qt::KeepAspectRatio));
};


#endif // BULLET_H
