#include "mainwindow.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <ctime>
#include <cstdlib>
#include "game.h"

Game * game;

int main(int argc, char *argv[])
{
       QApplication a(argc, argv);

       srandom(time(NULL));
       
       game = new Game();

       game->show();

       return a.exec();
}
