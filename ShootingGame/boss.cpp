#include "boss.h"
#define player_num 4
#define Amakusa 0
#define Emiya 1
//#define Helen 2
#define Illya 2
#define Kuro 3
#define sSound_num 2
//#define Scale 0.1
#define hpadS 10

extern Game * game;

Boss::Boss():Character()
{
    rand_ply=rand()%player_num;
    int rand_sply = rand()%sSound_num;

    switch (rand_ply)
    {
        case Amakusa:
        {
            QPixmap * playerPic = new QPixmap(":/amakusa/Amakusa3.png");
            QPixmap * playerPicScaled = new QPixmap();
            //qDebug()<<"Amakusa is created by"<<playerPic->width()<<" "<<playerPic->height()<<endl;
            //int picw=playerPic->width()*Scale;
            //int pich=playerPic->height()*Scale;
            *playerPicScaled = playerPic->scaled(playerPic->width()*Scale,playerPic->height()*Scale,Qt::KeepAspectRatio);
            //qDebug()<<"Amakusa is created by"<<playerPicScaled->width()<<" "<<playerPicScaled->height()<<endl;
            setPixmap(*playerPicScaled);
            delete playerPic;
            delete playerPicScaled;
            //setPixmap(QPixmap(":/amakusa/Amakusa2.png"));
            //setScale(Scale);
            //qDebug()<<"Amakusa is created at"<<this->pixmap().width()<<" "<<this->pixmap().height()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/amakusa/AmakusaS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/amakusa/AmakusaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=120*hpadS;
            ad=5*hpadS;
            //qDebug()<<"Character initialization is success"<<endl;
        break;
        }

        case Emiya:
        {
            QPixmap * playerPic = new QPixmap(":/emiya/Emiya3.png");
            QPixmap * playerPicScaled = new QPixmap();
            //qDebug()<<"Amakusa is created by"<<playerPic->width()<<" "<<playerPic->height()<<endl;
            //int picw=playerPic->width()*Scale;
            //int pich=playerPic->height()*Scale;
            *playerPicScaled = playerPic->scaled(playerPic->width()*Scale,playerPic->height()*Scale,Qt::KeepAspectRatio);
            //qDebug()<<"Amakusa is created by"<<playerPicScaled->width()<<" "<<playerPicScaled->height()<<endl;
            setPixmap(*playerPicScaled);
            delete playerPic;
            delete playerPicScaled;
            //setPixmap((QPixmap(":/emiya/Emiya2.png")));
            //setScale(Scale);
            //qDebug()<<"Emiya is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/emiya/Emiya.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/emiya/EmiyaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=100*hpadS;
            ad=10*hpadS;
        break;
        }

        /*case Helen:
        {
            setPixmap((QPixmap(":/helen/Helen2.png")));
            setScale(Scale);
            qDebug()<<"Helen is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/helen/HelenS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/helen/HelenS2.m4a"));
                break;
            }
            plyStart->play();
            break;
        }*/

        case Illya:
        {
            QPixmap * playerPic = new QPixmap(":/illya/Illya3.png");
            QPixmap * playerPicScaled = new QPixmap();
            //qDebug()<<"Amakusa is created by"<<playerPic->width()<<" "<<playerPic->height()<<endl;
            //int picw=playerPic->width()*Scale;
            //int pich=playerPic->height()*Scale;
            *playerPicScaled = playerPic->scaled(playerPic->width()*Scale,playerPic->height()*Scale,Qt::KeepAspectRatio);
            //qDebug()<<"Amakusa is created by"<<playerPicScaled->width()<<" "<<playerPicScaled->height()<<endl;
            setPixmap(*playerPicScaled);
            delete playerPic;
            delete playerPicScaled;
            //setPixmap((QPixmap(":/illya/Illya2.png")));
            //setScale(Scale);
            //qDebug()<<"Illya is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/illya/IllyaS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/illya/IllyaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=80*hpadS;
            ad=15*hpadS;
        break;
        }

        case Kuro:
        {
            QPixmap * playerPic = new QPixmap(":/kuro/Kuro3.png");
            QPixmap * playerPicScaled = new QPixmap();
            //qDebug()<<"Amakusa is created by"<<playerPic->width()<<" "<<playerPic->height()<<endl;
            //int picw=playerPic->width()*Scale;
            //int pich=playerPic->height()*Scale;
            *playerPicScaled = playerPic->scaled(playerPic->width()*Scale,playerPic->height()*Scale,Qt::KeepAspectRatio);
            //qDebug()<<"Amakusa is created by"<<playerPicScaled->width()<<" "<<playerPicScaled->height()<<endl;
            setPixmap(*playerPicScaled);
            delete playerPic;
            delete playerPicScaled;
            //setPixmap((QPixmap(":/kuro/Kuro2.png")));
            //setScale(Scale);
            //qDebug()<<"Kuro is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/kuro/KuroS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/kuro/KuroS2.m4a"));
                break;
            }
            plyStart->play();
            hp=60*hpadS;
            ad=20*hpadS;
        }
    }

    posx=this->x()+this->pixmap().width()/2;
    posy=this->y()+this->pixmap().height()/2;

    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));
    timer->start(1000);

    QTimer* timer2 =new QTimer();
    connect(timer2,SIGNAL(timeout()),this,SLOT(shootnow()));
    timer2->start(2000);
}

void Boss::shoot()
{
    Bullet * b;
    if(game->player->y()>=this->y()&&game->player->y()<=this->y()+this->pixmap().height())
         b = new Bullet(this->x()-10,game->player->y(),rand_ply,ad,bulletBool);
    else if(game->player->y()<this->y())
         b = new Bullet(this->x()-10,this->y(),rand_ply,ad,bulletBool);
    else if(game->player->y()>this->y()+this->pixmap().height())
         b = new Bullet(this->x()-10,this->y()+this->pixmap().height(),rand_ply,ad,bulletBool);
    scene()->addItem(b);
}

void Boss::move()
{
    if(this->getPosy()<game->player->getPosy())
    {
        setPos(x(),y()+15);
        setPosx();
        setPosy();
        qDebug()<<this->getPosy()<<" "<<game->player->getPosy()<<endl;
    }
    else
    {
        setPos(x(),y()-15);
        setPosx();
        setPosy();
        qDebug()<<this->getPosy()<<" "<<game->player->getPosy()<<endl;
    }
}

void Boss::shootnow()
{
    this->shoot();
}
