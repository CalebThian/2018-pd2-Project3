#ifndef NP_H
#define NP_H
#include <QObject>
#include <QTimer>
#include <QGraphicsTextItem>
#include <QFont>
#include <QGraphicsPixmapItem>
#include "game.h"

class NP: public QGraphicsTextItem
{
    Q_OBJECT

public:
    NP(QGraphicsItem *parent=0);
    void initialize();
    int getnp();

public slots:
    void increaseNP();

private:
    int np;
    const int npIncrement=1;
};

#endif // NP_H
