#-------------------------------------------------
#
# Project created by QtCreator 2018-05-31T09:31:43
#
#-------------------------------------------------

QT       += core gui \
         multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ShootingGame
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        mainwindow.cpp \
    character.cpp \
    bullet.cpp \
    enemy.cpp \
    main.cpp \
    game.cpp \
    score.cpp \
    health.cpp \
    np.cpp \
    boss.cpp \
    player.cpp \
    skill1.cpp \
    sktimer.cpp

HEADERS += \
        mainwindow.h \
    character.h \
    bullet.h \
    enemy.h \
    game.h \
    score.h \
    health.h \
    np.h \
    boss.h \
    player.h \
    skill1.h \
    sktimer.h

FORMS += \
        mainwindow.ui

RESOURCES += \
    res.qrc
