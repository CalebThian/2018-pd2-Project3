#include "bullet.h"
#define Amakusa 0
#define Emiya 1
//#define Helen 2
#define Illya 2
#define Kuro 3
#define AmakusaUlti1 4
#define AmakusaUlti2 5
#define EmiyaUlti 6
#define IllyaUlti 7
#define KuroUlti 8
#define bulletSoundEffect_num 16
#define hitSoundEffect_num 8

extern Game * game;


Bullet::Bullet()
{

    setPixmap(QPixmap(":/amakusa/AmakusaBullet.png"));

    QTimer * Timer= new QTimer();

    connect(Timer,SIGNAL(timeout()),this,SLOT(move()));

    Timer->start(50);

}

Bullet::Bullet(int a, int b,int c,int d,bool e)
{
    attack =d;
    playerOrBoss = e;
    orix=a;
    oriy=b;
    int rand_soundEffect = rand()%bulletSoundEffect_num;
    QMediaPlayer * bulletsound = new QMediaPlayer();
    switch(c)
    {
        case Amakusa:
        {
            npIncrement = 2;
            setPos(a,b);
            if(playerOrBoss==0)
            {
                setPixmap(*amakusaBulletScaled);
                setTransformOriginPoint(this->pixmap().width()/2,this->pixmap().height()/2);
                setRotation(180);
            }
            else
                setPixmap(*bAmakusaBulletScaled);
            switch(rand_soundEffect)
            {
            case 0:
                 bulletsound->setMedia(QUrl("qrc:/amakusa/AmakusaA1.m4a"));
                break;

            case 1:
                 bulletsound->setMedia(QUrl("qrc:/amakusa/AmakusaA2.m4a"));
                break;

            case 2:
                 bulletsound->setMedia(QUrl("qrc:/amakusa/AmakusaA3.m4a"));
                break;

            case 3:
                 bulletsound->setMedia(QUrl("qrc:/amakusa/AmakusaA4.m4a"));
                break;

            default:
                break;
            }
            if(bulletsound->state()==QMediaPlayer::PlayingState)
                bulletsound->setPosition(0);
            else if (bulletsound->state()==QMediaPlayer::StoppedState)
                bulletsound->play();
            break;
        }

        case Emiya:
        {
            npIncrement = 4;
            setPos(a,b);
            if(playerOrBoss==0)
            {
                setPixmap(*emiyaBulletScaled);
                setTransformOriginPoint(this->pixmap().width()/2,this->pixmap().height()/2);
                setRotation(180);
            }
            else
                setPixmap(*bEmiyaBulletScaled);
            switch(rand_soundEffect)
            {
            case 0:
                 bulletsound->setMedia(QUrl("qrc:/emiya/EmiyaA1.m4a"));
                break;

            case 1:
                 bulletsound->setMedia(QUrl("qrc:/emiya/EmiyaA2.m4a"));
                break;

            case 2:
                 bulletsound->setMedia(QUrl("qrc:/emiya/EmiyaA3.m4a"));
                break;

            case 3:
                 bulletsound->setMedia(QUrl("qrc:/emiya/EmiyaA4.m4a"));
                break;

            default:
                break;
            }
            if(bulletsound->state()==QMediaPlayer::PlayingState)
                bulletsound->setPosition(0);
            else if (bulletsound->state()==QMediaPlayer::StoppedState)
                bulletsound->play();
            break;
        }

        case Illya:
        {
            npIncrement = 8;
            setPos(a,b);
            if(playerOrBoss==0)
            {
                setPixmap(*illyaBulletScaled);
                setTransformOriginPoint(this->pixmap().width()/2,this->pixmap().height()/2);
                setRotation(180);
            }
            else
                setPixmap(*bIllyaBulletScaled);
            switch(rand_soundEffect)
            {
            case 0:
                 bulletsound->setMedia(QUrl("qrc:/illya/IllyaA1.m4a"));
                break;

            case 1:
                 bulletsound->setMedia(QUrl("qrc:/illya/IllyaA2.m4a"));
                break;

            case 2:
                 bulletsound->setMedia(QUrl("qrc:/illya/IllyaA3.m4a"));
                break;

            case 3:
                 bulletsound->setMedia(QUrl("qrc:/illya/IllyaA4.m4a"));
                break;

            default:
                break;
            }
            if(bulletsound->state()==QMediaPlayer::PlayingState)
                bulletsound->setPosition(0);
            else if (bulletsound->state()==QMediaPlayer::StoppedState)
                bulletsound->play();
            break;
        }

        case Kuro:
        {
            npIncrement = 6;
            setPos(a,b);
            if(playerOrBoss==0)
            {
                setPixmap(*kuroBulletcScaled);
                setTransformOriginPoint(this->pixmap().width()/2,this->pixmap().height()/2);
                setRotation(180);
            }
            else
                setPixmap(*bKuroBulletcScaled);
            switch(rand_soundEffect)
            {
            case 0:
                 bulletsound->setMedia(QUrl("qrc:/kuro/KuroA1.m4a"));
                break;

            case 1:
                 bulletsound->setMedia(QUrl("qrc:/kuro/KuroA2.m4a"));
                break;

            case 2:
                 bulletsound->setMedia(QUrl("qrc:/kuro/KuroA3.m4a"));
                break;

            case 3:
                 bulletsound->setMedia(QUrl("qrc:/kuro/KuroA4.m4a"));
                break;

            default:
                break;
            }
            if(bulletsound->state()==QMediaPlayer::PlayingState)
                bulletsound->setPosition(0);
            else if (bulletsound->state()==QMediaPlayer::StoppedState)
                bulletsound->play();
            break;
        }

        case AmakusaUlti1:
        {
            npIncrement = 0;
            setPos(a,b);
            setPixmap(*amakusaUltiBulletScaled1);
            break;
        }

        case AmakusaUlti2:
        {
            npIncrement = 0;
            setPos(a,b);
            setPixmap(*amakusaUltiBulletScaled2);
            break;
        }

        case EmiyaUlti:
        {
            npIncrement = 2;
            setPos(a,b);
            setPixmap(*emiyaUltiBulletScaled);
            setTransformOriginPoint(this->pixmap().width()/2,this->pixmap().height()/2);
            setRotation(180);
            break;
        }

        case IllyaUlti:
        {
            npIncrement = 0;
            setPos(a,b);
            setPixmap(*illyaUltiBulletScaled);
            break;
        }

        case KuroUlti:
        {
            npIncrement = 1;
            setPos(a,b);
            setPixmap(*kuroUltiBulletScaled);
            setTransformOriginPoint(this->pixmap().width()/2,this->pixmap().height()/2);
            setRotation(180);
            break;
        }
    }
    QTimer * Timer= new QTimer();
    connect(Timer,SIGNAL(timeout()),this,SLOT(move()));

    Timer->start(50);
}

void Bullet::HitMusic(int a)
{
    //hit music
    QMediaPlayer * hitM = new QMediaPlayer();
    int r = rand()%hitSoundEffect_num;
    switch(a)
    {
        case Amakusa:
        {
            switch(r)
            {
                case 0:
                {
                    hitM->setMedia(QUrl("qrc:/amakusa/AmakusaB1.m4a"));
                    break;
                }

                case 1:
                {
                    hitM->setMedia(QUrl("qrc:/amakusa/AmakusaB2.m4a"));
                    break;
                }
            }
            if(hitM->state()==QMediaPlayer::PlayingState)
                hitM->setPosition(0);
            else if (hitM->state()==QMediaPlayer::StoppedState)
                hitM->play();
            break;
        }

        case Emiya:
        {
            switch(r)
            {
                case 0:
                {
                    hitM->setMedia(QUrl("qrc:/emiya/EmiyaB1.m4a"));
                    break;
                }

                case 1:
                {
                    hitM->setMedia(QUrl("qrc:/emiya/EmiyaB2.m4a"));
                    break;
                }
            }
            if(hitM->state()==QMediaPlayer::PlayingState)
                hitM->setPosition(0);
            else if (hitM->state()==QMediaPlayer::StoppedState)
                hitM->play();
            break;
        }

        case Illya:
        {
            switch(r)
            {
                case 0:
                {
                    hitM->setMedia(QUrl("qrc:/illya/IllyaB1.m4a"));
                    break;
                }

                case 1:
                {
                    hitM->setMedia(QUrl("qrc:/illya/IllyaB2.m4a"));
                    break;
                }
            }
            if(hitM->state()==QMediaPlayer::PlayingState)
                hitM->setPosition(0);
            else if (hitM->state()==QMediaPlayer::StoppedState)
                hitM->play();
            break;
        }

        case Kuro:
        {
            switch(r)
            {
                case 0:
                {
                    hitM->setMedia(QUrl("qrc:/kuro/KuroB1.m4a"));
                    break;
                }

                case 1:
                {
                    hitM->setMedia(QUrl("qrc:/kuro/KuroB2.m4a"));
                    break;
                }
            }
            if(hitM->state()==QMediaPlayer::PlayingState)
                hitM->setPosition(0);
            else if (hitM->state()==QMediaPlayer::StoppedState)
                hitM->play();
            break;
        }
    }
}

Bullet::~Bullet()
{

}

void Bullet::move()
{
 QList<QGraphicsItem *> collided_Items = collidingItems();
 for(int i=0,n=collided_Items.size();i<n;i++)
 {
     if(playerOrBoss==1)
     {
         if(typeid(*(collided_Items[i])) == typeid(Player))
        {
            for(int j=0;j<attack;++j)
                game->health->decrease();
            for(int j=0;j<12000/this->pixmap().height();++j)
                game->score->decrease();
            game->hpblock->setRect(game->hpx,game->hpy,game->health->getHealth(),20);

            for(int k=0;k<npIncrement;++k)
                game->bnp->increaseNP();

            scene()->removeItem(this);
            delete this;

            HitMusic(game->player->getPly());

            return;
        }
     }
     else
     {
        if(typeid(*(collided_Items[i])) == typeid(Boss))
        {
            for(int k=0;k<120/this->pixmap().height()+orix;++k)
                game->score->increase();
            for(int j=0;j<attack/2;++j)
                game->bhealth->decrease();
            game->bhpblock->setRect(0,0,game->bhealth->getHealth(),20);

            for(int k=0;k<npIncrement;++k)
                game->np->increaseNP();
            scene()->removeItem(this);
            delete this;

            HitMusic(game->boss->getPly());

            return;

        }
     }
 }
 if(playerOrBoss==0)
 {
    setPos(x()+10,y());
    if(pos().x()>scene()->width())
    {
        scene()->removeItem(this);
        delete this;
    }
 }
 else
 {
    setPos(x()-10,y());
    if(pos().x()+this->pixmap().width()<0)
    {
        scene()->removeItem(this);
        delete this;
    }
}
}
