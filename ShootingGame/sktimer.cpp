#include "sktimer.h"

SkTimer::SkTimer(int a)
{
    initial = a;
    counter = 0;
    if(a==0)
    {
        setPlainText(QString("X"));
        setDefaultTextColor(Qt::black);
        setFont(QFont("times",120));
    }
    else
    {
        setPlainText(QString(""));
        setDefaultTextColor(Qt::white);
        setFont(QFont("times",20));
    }
}

void SkTimer::decrease()
{
    if(counter > 0)
    {
        --counter;
        setPlainText(QString::number(counter));
    }
    else if(counter == 0)
        setPlainText(QString(""));
}

void SkTimer::cannot()
{
    setPlainText("X");
}

void SkTimer::can()
{
    setPlainText("");
}

void SkTimer::initialize()
{
    counter = initial;
     setPlainText(QString::number(counter));
}

int SkTimer::getInitial()
{
    return initial;
}

int SkTimer::getCounter()
{
    return counter;
}
