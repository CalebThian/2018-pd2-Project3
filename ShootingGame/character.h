#ifndef CHARACTER_H
#define CHARACTER_H
#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QKeyEvent>
#include <Qt>
#include <QObject>
#include <QGraphicsScene>
#include <cstdlib>
#include <QTimer>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QDebug>

class Character : public QObject , public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Character();
    void keyPressEvent(QKeyEvent *e);
    int gethp();
    float getPosx();
    float getPosy();
    int getad();
    void setPosx();
    void setPosy();
    void move_left();
    void move_right();
    void move_up();
    void move_down();
    int getPly();
    virtual void shoot()=0;
    virtual void skill1();
    virtual void skill2();
    virtual void ultimate();
    //virtual void rotateornot()=0;
    //virtual void changeScale()=0;
    QGraphicsRectItem * hpblock;

public slots:
   // void spawn();
    virtual void count1Decrease();
    virtual void count2Decrease();

protected:
    int rand_ply;
    int hp;
    int ad;
    float posx;
    float posy;
    float Scale = 0.1;
};

#endif // CHARACTER_H
