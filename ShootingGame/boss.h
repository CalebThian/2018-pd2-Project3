#ifndef BOSS_H
#define BOSS_H
#include "character.h"
#include <QObject>
#include "game.h"

class Character;

class Game;

class Boss:public Character
{
    Q_OBJECT

public:
    Boss();
    virtual void shoot();

public slots:
    void move();
    void shootnow();

protected:
    double Scale = 0.2;
    const bool bulletBool=1;
};

#endif // BOSS_H
