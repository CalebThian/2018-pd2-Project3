
#include "game.h"

   Game::Game(QWidget *parent)
   {
       end =0;
       //Create a character pointer as player
       //player =new Character();
       player = new Player();

       //Create a boss pointer boss
       boss = new Boss();

       //Create character hpblock
       hpblock = new QGraphicsRectItem();
       bhpblock = new QGraphicsRectItem();

       //Create a QGraphicsScene Pointer
       scene = new QGraphicsScene();
       QPixmap * bg = new QPixmap(":/bg/fgobg.png");
       //QSize * bgsize = new QSize(w,h);
       QPixmap * bgscaled = new QPixmap();
       * bgscaled = bg->scaled(w,h,Qt::IgnoreAspectRatio);
       setBackgroundBrush(QBrush(*bgscaled));

       //Create a Score pointer
       score = new Score();

       //Create a Health pointer
       health = new Health(player->gethp());
       bhealth = new Health(boss->gethp());

       //Create a np and a bnp pointer
       np = new NP();
       bnp = new NP();

       //set player position
       //player->setRect(0,0,20,20);
       player->setPos(0,0);
       boss->setPos(100,0);
       hpblock->setRect(w/2-player->gethp()/2,h-50,player->gethp(),20);
       bhpblock->setRect(0,0,boss->gethp(),20);
       hpx = w/2-player->gethp()/2;
       hpy =h -50;

       //set np position
       np->setPos(w/2-player->gethp()/2,h-30);
       bnp->setPos(0,20);

       //set Score position
       score->setPos(w/2-player->gethp()/2+200,h-30);

       //set player flag and boss flag to focusable
       player->setFlag(QGraphicsItem::ItemIsFocusable);
       boss->setFlag(QGraphicsItem::ItemIsFocusable);

       //set player to be the focus item
       player->setFocus();
       //boss->setFocus();

       //create skill icon and set their position
       skill1 = new Skill1(0);
       skill2 = new Skill1(1);
       skill1->setPos(w-120,h-60);
       skill2->setPos(w-60,h-60);

       //initialize ss
       QPixmap * SC ;
       switch(player->getPly())
       {
           case 0:
           {
               SC = new QPixmap(":/amakusa/AmakusaSS.jpg");
               break;
           }
           case 1:
           {
               SC = new QPixmap(":/emiya/EmiyaSS.jpg");
               break;
           }
           case 2:
           {
               SC = new QPixmap(":/illya/IllyaSS.jpg");
               break;
           }
           case 3:
           {
               SC = new QPixmap(":/kuro/KuroSS.jpg");
               break;
           }
       }
       QPixmap * SCS = new QPixmap();
       *SCS = SC->scaled(140,160,Qt::KeepAspectRatio);
       SCSC = new QGraphicsPixmapItem();
       SCSC->setPixmap(*SCS);
       SCSC->setPos(skill1->x()-(20+140),h-170);
       delete SC;
       delete SCS;

       //create SkTimer pointer and set their position
       skt1 = new SkTimer(skill1->getCooldown());
       skt2 = new SkTimer(skill2->getCooldown());
       skt3 = new SkTimer(0);
       skt1->setPos(w-120+skill1->pixmap().width()/4,h-60+skill1->pixmap().height()/4);
       skt2->setPos(w-60+skill2->pixmap().width()/4,h-60+skill2->pixmap().height()/4);
       skt3->setPos(w-130-SCSC->pixmap().width(),SCSC->y()-10);

       //create skill counter and set their position
//       skillcounter = new QPixmap(":/bg/skillcounter3.png");
//       QPixmap * skillcounterScaled = new QPixmap();
//       *skillcounterScaled = skillcounter->scaled(20,20,Qt::IgnoreAspectRatio);
//       QGraphicsPixmapItem * skCounter1 = new QGraphicsPixmapItem(*skillcounterScaled);
//       QGraphicsPixmapItem * skCounter2 = new QGraphicsPixmapItem(*skillcounterScaled);
//       QGraphicsPixmapItem * skCounter3 = new QGraphicsPixmapItem(*skillcounterScaled);
//       QGraphicsPixmapItem * skCounter4 = new QGraphicsPixmapItem(*skillcounterScaled);
//       skCounter1->setPos(skill1->x()+5,skill1->y()-25);
//       skCounter2->setPos(skill1->x()+25,skill1->y()-25);
//       skCounter3->setPos(skill2->x()+5,skill2->y()-25);
//       skCounter4->setPos(skill2->x()+25,skill2->y()-25);

       //Add player,boss,np and score to scene
       scene->addItem(score);
       scene->addItem(boss);
       scene->addItem(player);
       scene->addItem(hpblock);
       scene->addItem(bhpblock);
       scene->addItem(np);
       scene->addItem(bnp);
       scene->addItem(skill1);
       scene->addItem(skill2);
       scene->addItem(SCSC);
       scene->addItem(skt1);
       scene->addItem(skt2);
       scene->addItem(skt3);
//       scene->addItem(skCounter1);
//       scene->addItem(skCounter2);
//       scene->addItem(skCounter3);
//       scene->addItem(skCounter4);

      //Off scroll bar
      setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
      setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

       //Add scene to view;
       setScene(scene);

      //Set scene and view size and in same coordinate system
      setFixedSize(w,h);
      scene->setSceneRect(0,0,w,h);
      player->setPos(0,this->height()/2-player->pixmap().height()/2 /*+ player->pixmap().height()*0.1/2*/);
      boss->setPos(w-boss->pixmap().width(),this->height()/2-boss->pixmap().height()/2);
      qDebug()<<"boss is set at"<<boss->x()<<" "<<boss->y()<<endl;

      //qDebug()<<"hpblock is located at "<<hpblock->rect().x()<<" "<<hpblock->rect().y();



      //play bgm
      //play bgm in mode loop
      //QMediaPlaylist * playlist;
      //playlist->addMedia(QUrl("qrc:/bgm/battle1.mp4"))
      //playlist->setPlaybackMode(QMediaPlaylist::Loop);
      bgm = new QMediaPlayer();
      bgm->setMedia(QUrl("qrc:/bgm/battle1.mp4"));
      //bgm->setPlaylist(playlist);
      bgm->play();

      QTimer * Timer= new QTimer();
      connect(Timer,SIGNAL(timeout()),this,SLOT(loopbgm()));
      Timer->start(10000);
      //qDebug()<<"loopbgm slot is not the matter"<<endl;

      /*
      //random generate enemy in constant time interval
      QTimer * timer = new QTimer();
      connect(timer,SIGNAL(timeout()),player,SLOT(spawn()));
      timer->start(2000);
      */

      //check game over
      QTimer * timer = new QTimer();
      connect(timer,SIGNAL(timeout()),this, SLOT(gameover()));
      timer->start(100);

      //show view

      show();
   }



   void Game::loopbgm()
   {
       if(bgm->state()==QMediaPlayer::StoppedState)
       {
           bgm->setPosition(0);
           bgm->play();
       }
   }

   void Game::gameover()
   {
       if(end == 0)
       {
           QPixmap * result= new QPixmap();
           QPixmap * win = new QPixmap(":/bg/victory.png");
           QPixmap * lose = new QPixmap(":/bg/defeat.png");

           QMediaPlayer * resultAudio = new QMediaPlayer();
           int rAudio = rand()%2;
           if(bhealth->getHealth()<=0)
           {
              bgm->pause();
              *result = win->scaled(w,h,Qt::KeepAspectRatio);
              switch(player->getPly())
              {
                case 0:
                {
                  switch(rAudio)
                  {
                  case 0:
                      resultAudio->setMedia(QUrl("qrc:/amakusa/AmakusaW1.m4a"));
                      break;

                  case 1:
                      resultAudio->setMedia(QUrl("qrc:/amakusa/AmakusaW2.m4a"));
                      break;
                  }
                  break;
              }

              case 1:
              {
                switch(rAudio)
                {
                case 0:
                    resultAudio->setMedia(QUrl("qrc:/emiya/EmiyaW1.m4a"));
                    break;

                case 1:
                    resultAudio->setMedia(QUrl("qrc:/emiya/EmiyaW2.m4a"));
                    break;
                }
                break;
              }

              case 2:
              {
                switch(rAudio)
                {
                case 0:
                    resultAudio->setMedia(QUrl("qrc:/illya/IllyaW1.m4a"));
                    break;

                case 1:
                    resultAudio->setMedia(QUrl("qrc:/illya/IllyaW2.m4a"));
                    break;
                }
                break;
              }

              case 3:
              {
                switch(rAudio)
                {
                case 0:
                    resultAudio->setMedia(QUrl("qrc:/kuro/KuroW1.m4a"));
                    break;

                case 1:
                    resultAudio->setMedia(QUrl("qrc:/kuro/KuroW2.m4a"));
                    break;
                }
                break;
              }

            }
              resultAudio->play();
              Score * finalScore = new Score();
              finalScore->setScore(score->getScore());
              scene->clear();
              scene->clearFocus();
              score->setPos(w/2-50,h-200);
              QGraphicsPixmapItem * res = new QGraphicsPixmapItem();
              res->setPixmap(*result);
              res->setPos(w/2-res->pixmap().width()/2,0);
              scene->addItem(res);
              scene->addItem(finalScore);
              end=1;
           }

           else if(health->getHealth()<=0)
           {
              bgm->pause();
              *result = lose->scaled(w,h,Qt::KeepAspectRatio);

              switch(player->getPly())
              {
                case 0:
                {
                  switch(rAudio)
                  {
                  case 0:
                      resultAudio->setMedia(QUrl("qrc:/amakusa/AmakusaL1.m4a"));
                      break;

                  case 1:
                      resultAudio->setMedia(QUrl("qrc:/amakusa/AmakusaL2.m4a"));
                      break;
                  }
                  break;
              }

              case 1:
              {
                switch(rAudio)
                {
                case 0:
                    resultAudio->setMedia(QUrl("qrc:/emiya/EmiyaL1.m4a"));
                    break;

                case 1:
                    resultAudio->setMedia(QUrl("qrc:/emiya/EmiyaL2.m4a"));
                    break;
                }
                break;
              }

              case 2:
              {
                switch(rAudio)
                {
                case 0:
                    resultAudio->setMedia(QUrl("qrc:/illya/IllyaL1.m4a"));
                    break;

                case 1:
                    resultAudio->setMedia(QUrl("qrc:/illya/IllyaL2.m4a"));
                    break;
                }
                break;
              }

              case 3:
              {
                switch(rAudio)
                {
                case 0:
                    resultAudio->setMedia(QUrl("qrc:/kuro/KuroL1.m4a"));
                    break;

                case 1:
                    resultAudio->setMedia(QUrl("qrc:/kuro/KuroL2.m4a"));
                    break;
                }
                break;
              }

            }
              resultAudio->play();
              Score * finalScore = new Score();
              finalScore->setScore(score->getScore());
              scene->clear();
              scene->clearFocus();
              finalScore->setPos(w/2-50,h-200);
              QGraphicsPixmapItem * res = new QGraphicsPixmapItem();
              res->setPixmap(*result);
              res->setPos(w/2-res->pixmap().width()/2,0);
              scene->addItem(res);
              scene->addItem(finalScore);
              end=1;
           }
       }
   }


