#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QGraphicsScene>
#include <QBrush>
#include <QGraphicsView>
#include <cstdlib>
#include <QTimer>
#include <ctime>
#include <QSize>
#include <QPixmap>
#include <QDebug>
#include <QMediaPlayer>
#include <QObject>
//#include "character.h"
#include "score.h"
#include "health.h"
#include "np.h"
#include "bullet.h"
#include "boss.h"
#include "player.h"
//#include "skill.h"
#include "skill1.h"
#include "sktimer.h"


class Character;
class Boss;
class Player;
class Skill1;
class NP;

class Game: public QGraphicsView
{
    Q_OBJECT

public:

    QGraphicsScene * scene;
    //Character * player;
    Player * player;
    Boss * boss;
    Score * score;
    Health * health;
    Health * bhealth;
    QGraphicsRectItem * hpblock;
    QGraphicsRectItem * bhpblock;
    QGraphicsPixmapItem * SCSC;
    NP * np;
    NP * bnp;
    Skill1 * skill1;
    Skill1 * skill2;
    SkTimer * skt1;
    SkTimer *skt2;
    SkTimer * skt3;

    int hpx;//hpblock x pos
    int hpy;//hpblock y pos
    const int w =1200;//width
    const int h =800;//height
    QMediaPlayer * bgm;
    Game(QWidget * parent =0 );
//    QPixmap * skillcounter;
//    QGraphicsPixmapItem * skCounter1 = new QGraphicsPixmapItem(*skillcounterScaled);
//    QGraphicsPixmapItem * skCounter2 = new QGraphicsPixmapItem(*skillcounterScaled);
//    QGraphicsPixmapItem * skCounter3 = new QGraphicsPixmapItem(*skillcounterScaled);
//    QGraphicsPixmapItem * skCounter4 = new QGraphicsPixmapItem(*skillcounterScaled);

public slots:
        void loopbgm();
        void gameover();

private:
    bool end;

};

#endif // ENEMY_H
