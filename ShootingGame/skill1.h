#ifndef SKILL1_H
#define SKILL1_H

#include <QGraphicsRectItem>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QObject>
#include <QDebug>
#include "game.h"

class Game;

class Skill1 : public QGraphicsPixmapItem
{
public:
//    Skill1(int a, int b);
    Skill1(int a);
    int getCooldown();

private:
    int cooldown;
};

#endif // SKILL1_H
