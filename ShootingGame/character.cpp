#include "character.h"
#define player_num 4
#define Amakusa 0
#define Emiya 1
//#define Helen 2
#define Illya 2
#define Kuro 3
#define sSound_num 2
//#define Scale 0.1
#define hpadS 5


Character::Character()
{
    //rand_ply = rand()%player_num;
    /*rand_ply=0;
    int rand_sply = rand()%sSound_num;

    switch (rand_ply)
    {
        case Amakusa:
        {
            setPixmap(QPixmap(":/amakusa/Amakusa2.png"));
            setScale(Scale);
            qDebug()<<"Amakusa is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/amakusa/AmakusaS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/amakusa/AmakusaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=120*hpadS;
            ad=5*hpadS;
            qDebug()<<"Character initialization is success"<<endl;
        break;
        }

        case Emiya:
        {
            setPixmap((QPixmap(":/emiya/Emiya2.png")));
            setScale(Scale);
            qDebug()<<"Emiya is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/emiya/Emiya.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/emiya/EmiyaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=100*hpadS;
            ad=10*hpadS;
        break;
        }

        case Helen:
        {
            setPixmap((QPixmap(":/helen/Helen2.png")));
            setScale(Scale);
            qDebug()<<"Helen is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/helen/HelenS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/helen/HelenS2.m4a"));
                break;
            }
            plyStart->play();
            break;
        }

        case Illya:
        {
            setPixmap((QPixmap(":/illya/Illya2.png")));
            setScale(Scale);
            qDebug()<<"Illya is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/illya/IllyaS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/illya/IllyaS2.m4a"));
                break;
            }
            plyStart->play();
            hp=80*hpadS;
            ad=15*hpadS;
        break;
        }

        case Kuro:
        {
            setPixmap((QPixmap(":/kuro/Kuro2.png")));
            setScale(Scale);
            qDebug()<<"Kuro is created at"<<this->x()<<" "<<this->y()<<endl;
            QMediaPlayer * plyStart = new QMediaPlayer();
            switch(rand_sply)
            {
                case 0:
                    plyStart->setMedia(QUrl("qrc:/kuro/KuroS1.m4a"));
                break;

                case 1:
                    plyStart->setMedia(QUrl("qrc:/kuro/KuroS2.m4a"));
                break;
            }
            plyStart->play();
            hp=60*hpadS;
            ad=20*hpadS;
        }
    }

    posx=this->x()+this->pixmap().width()/2-this->pixmap().width()*Scale*Scale;
    posy=this->y()+this->pixmap().height()/2-this->pixmap().height()*Scale*Scale;
*/
   /* QTimer * Timer2= new QTimer();
    connect(Timer2,SIGNAL(timeout()),this,SLOT(spawn()));
    Timer2->start(2000);*/
}

void Character::keyPressEvent(QKeyEvent * e)
{
    switch (e->key())
    {
    case Qt::Key_Left:
        move_left();
        break;

    case Qt::Key_Right:
        move_right();
        break;


    case Qt::Key_Up:
        move_up();
        break;

    case Qt::Key_Down:
        move_down();
        break;

    case Qt::Key_Space:
        shoot();
        break;

    case Qt::Key_Q:
        skill1();
        break;

    case Qt::Key_W:
        skill2();
        break;

    case Qt::Key_R:
        ultimate();
        break;
    }


}

int Character::gethp()
{
    return hp;
}

float Character::getPosx()
{
    setPosx();
    return posx;
}

float Character::getPosy()
{
    setPosy();
    return posy;
}

int Character::getad()
{
    return ad;
}

void Character::setPosx()
{
    posx=this->x()+this->pixmap().width()/2;
}

void Character::setPosy()
{
    posy=this->y()+this->pixmap().height()/2;
}

void Character::move_left()
{
    if(pos().x()>0)
        this->setPos(x()-10,y());
    this->setPosx();
    this->setPosy();
}

void Character::move_right()
{
    if(pos().x()+this->pixmap().width()<scene()->width())
        this->setPos(x()+10,y());
    this->setPosx();
    this->setPosy();
}

void Character::move_up()
{
    if(pos().y()>0)
        this->setPos(x(),y()-10);
    this->setPosx();
    this->setPosy();
}

void Character::move_down()
{
    if(pos().y()+this->pixmap().height()<scene()->height())
        this->setPos(x(),y()+10);
    this->setPosx();
    this->setPosy();
}

int Character::getPly()
{
    return rand_ply;
}

void Character::skill1()
{

}

void Character::skill2()
{

}

void Character::ultimate()
{

}

void Character::count1Decrease()
{

}

void Character::count2Decrease()
{

}

/*void Character::shoot()
{
    Bullet * b = new Bullet(this->x()+this->pixmap().width()*Scale,this->y()+this->pixmap().height()*Scale/2,rand_ply,ad);
    scene()->addItem(b);
}*/

/*void Character::spawn()
{
    Enemy * enemy = new Enemy();
    scene()->addItem(enemy);
}
*/
