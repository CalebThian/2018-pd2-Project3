#include "score.h"

Score::Score(QGraphicsItem *parent):QGraphicsTextItem(parent)
{
    score = 0;
    setPlainText(QString("Score: ")+QString::number(score));
    setDefaultTextColor(Qt::blue);
    setFont(QFont("times",16));
}

void Score::increase()
{
    ++score;
    setPlainText(QString("Score: ")+QString::number(score));
}

void Score::decrease()
{
    --score;
    setPlainText(QString("Score: ")+QString::number(score));
}

int Score::getScore()
{
    return score;
}

void Score::setScore(int a)
{
    score = a;
    setPlainText(QString("Score: ")+QString::number(score));
}


