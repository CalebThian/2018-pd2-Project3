#include "health.h"


Health::Health(QGraphicsItem *parent):QGraphicsTextItem (parent)
{
    health = 4;
    setPlainText(QString ("Health: ") + QString::number(health));

    setDefaultTextColor("red");
    setFont(QFont("times",16));
}

Health::Health(int a)
{
    health = a;
}

void Health::decrease()
{
    --health;
    //setPlainText(QString ("Health: ") + QString::number(health));
}

void Health::increase()
{
    ++health;
}

int Health::getHealth()
{
    return health;
}

