#ifndef SKILL_H
#define SKILL_H
#include <QGraphicsRectItem>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include "game.h"
#include "skill1.h"

class Game;

class Skill: public QGraphicsPixmapItem
{
public:
    Skill(int a);
};

#endif // SKILL_H
