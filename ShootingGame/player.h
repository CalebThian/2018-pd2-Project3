#ifndef PLAYER_H
#define PLAYER_H
#include <QMediaPlayer>
#include "character.h"
#include "bullet.h"
#include "sktimer.h"
#include "game.h"

class Character;

class Player: public Character
{
public:
    Player();
    virtual void shoot();
    virtual void skill1();
    virtual void skill2();
    virtual void ultimate();

 public slots:
    void count1Decrease();
    void count2Decrease();

protected:
    const bool bulletBool=0;

private:
    QMediaPlayer * SSsound;
};

#endif // PLAYER_H
