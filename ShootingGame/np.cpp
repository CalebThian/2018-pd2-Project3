#include "np.h"

extern Game * game;

NP::NP(QGraphicsItem *parent):QGraphicsTextItem(parent)
{
    np =0;
    setPlainText(QString("NP: ")+QString::number(np));
    setDefaultTextColor(Qt::green);
    setFont(QFont("times",16));

    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(increaseNP()));
    timer->start(1000);
}

void NP::initialize()
{
    np=0;
    game->skt3->cannot();
}

int NP::getnp()
{
    return np;
}

void NP::increaseNP()
{
    if(np<100)
        ++np;
    setPlainText(QString("NP: ")+QString::number(np));
    if(np==100)
    {
        game->skt3->can();
    }
}


